package chapter03_04_05;

import java.time.LocalTime;
import java.util.Random;
import java.util.Scanner;

public class LearningTypes {
	/** Fun with types
     */
    public static void descInt(String args[]) {
        System.out.format("The 'byte' type is the shortest type (coded on 1 byte / 8 bits). It ranges from %d ("
                          + Integer.toBinaryString(Byte.MIN_VALUE).substring(Integer.SIZE - Byte.SIZE)
                          + ") to %d ("
                          + Integer.toBinaryString(Byte.MAX_VALUE)
                          + ")%n", Byte.MIN_VALUE, Byte.MAX_VALUE);
        System.out.format("The 'short' type is coded on 2 bytes / 16 bits). It ranges from %,d ("
                          + Integer.toBinaryString(Short.MIN_VALUE).substring(Integer.SIZE - Short.SIZE)
                          + ") to %,d ("
                          + Integer.toBinaryString(Short.MAX_VALUE)
                          + ")%n", Short.MIN_VALUE, Short.MAX_VALUE);
        System.out.format("The 'int' type is coded on 4 bytes / 32 bits). It ranges from %,d ("
                          + Integer.toBinaryString(Integer.MIN_VALUE)
                          + ") to %,d ("
                          + Integer.toBinaryString(Integer.MAX_VALUE)
                          + ")%n", Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.format("The 'long' type is coded on 8 bytes / 64 bits). It ranges from %,d to %,d%n",
                          Long.MIN_VALUE,
                          Long.MAX_VALUE);
        System.out.format("NOTE: The 'char' type is coded on 2 bytes but as an unsigned integer. "
                          + "It can be automatically cast as integer in computation.",
                          Long.MIN_VALUE,
                          Long.MAX_VALUE);
    }
    public static void descFloat(String args[]) {
        System.out.format("The 'float' type is coded on 4 bytes / 32 bits). It ranges from %1.4E to %1.4E%n",
                          Float.MIN_VALUE,
                          Float.MAX_VALUE);
        System.out.format("The 'double' type is coded on 8 bytes / 64 bits). It ranges from %1.4E to %1.4E%n",
                          Double.MIN_VALUE,
                          Double.MAX_VALUE);
        System.out.println("We can divide a float by zero. It just gives : "
                           + 2.3 / 0    // division has priority over sum, no need for parentheses
                           + ". The same result as out of bounds values.");
    }
    public static void magicConversion(String args[]) {
        int _int = 100;
        long _long = 100;
        float _float = 100.0f;
        Object obj = _int * _int;      // Resolves logically as int
        System.out.println("(int) 100 * (int) 100 = " + obj.toString() + " is " + obj.getClass());
        obj = _int * _long;            // Implicitly converts the integer to be coherent
        System.out.println("(int) 100 * (long) 100 = " + obj.toString() + " is " + obj.getClass());
        obj = _int * _long + _float;   /* Same as above but converts the product as float
                                        * Note: a float can represent an integer value but the opposite is not true
                                        */
        System.out.println("(int) 100 * (long) 100 + (float) 100 = " + obj.toString() + " is " + obj.getClass());
        int a = 234567891;
        float b;
        b = a;
        int _b = (int) b;    // Casting emulates automatic conversion
        System.out.println("WARNING: int -> float conversion imprecision: 234,567,891 - 234,567,891 = " + (_b - a));
    }
    public static void castErrors(String args[]) {
        int fits = 1000;
        byte does_not_fit;
        does_not_fit = (byte) fits;    // Basic case of overflow, only the least significant bits are stored
        System.out.println("(int -> byte) 1000 = " + does_not_fit);
        does_not_fit = 127;    // Legal
        System.out.println("(byte) 127 + 1 = " + ++does_not_fit);    /* Note: this works because increment is prefixed
                                                                      *     does_not_fit++ would have returned 127 and
                                                                      *     only subsequent calls would have shown 128
                                                                      */
    }
    public static void battleSimulator(String args[]) {
        int hp = 10;
        int enemy = 15;
        Random rnd = new Random();
        do {    // Could have been done as `while (hp > 0 || enemy > 0) {}`
            int enemy_attack = rnd.nextInt(4);
            hp -= enemy_attack;
            System.out.print("The monster attacks");
            if (enemy_attack == 0) System.out.println(" but fails!");
            else System.out.format(" and deals %d damage! You have %d HP left.%n", enemy_attack, Math.max(hp, 0));
            if (hp <= 0) {
                System.out.println("You died...");
                break;
            }
            int attack = rnd.nextInt(5);
            attack += (attack % 2 == 0) ? 1 : 0;  // Critical hit
            enemy -= attack;
            System.out.print("You attack");
            if (attack == 0) System.out.println(" but fail to land a hit!");
            else System.out.format(" and deal %d damage to the monster!%n", attack);
            if (enemy <= 0) {
                System.out.println("The monster falls down and exhale its last breath. Congratulations!");
                break;
            }
        } while (hp > 0 || enemy > 0);
        System.out.println("GAME OVER");
    }
    public static void main(String args[]) {
        Scanner input = new Scanner (System.in) ;
        System.out.println("Choose what you want to do:");
        System.out.println("1: Learn about integer types");
        System.out.println("2: Learn about floating point types");
        System.out.println("3: See how the compiler automatically converts type for coherence");
        System.out.println("4: See the possible errors when casting");
        System.out.println("5: Play a session of Battle Simulator");    // Just a do while exercise
        int choice = input.nextInt();    // At this point, the exception raised if not an integer is not handled
        input.close();
        switch (choice) {
            case 1  : descInt(args);
                      break;
            case 2 :  descFloat(args);
                      break;
            case 3 :  magicConversion(args);
                      break;
            case 4 :  castErrors(args);
                      break;
            case 5 :  battleSimulator(args);
                      break;
            default : LocalTime curr = LocalTime.now();
                      LocalTime noon = LocalTime.parse("12:00:00");
                      LocalTime dusk = LocalTime.parse("17:00:00");
                      /**
                       * The following syntax is correct, no need for blocks when dealing with single expressions
                       * But be careful while reading the code with indentation issues: the first else always relates
                       * to the the latest if
                       */
                      if (dusk.compareTo(curr) > 0) if (noon.compareTo(curr) > 0) System.out.println("Good morning");
                      else System.out.println("Good afternoon");
                      else System.out.println("Good evening");
                      break;
        }
    }
}
