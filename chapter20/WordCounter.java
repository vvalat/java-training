package chapter20;

import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

class WordCountProcessor {
    public WordCountProcessor(Path file){
        this.file = file;
    }
    public void process() throws FileNotFoundException {
        /* Populates the word list. 99% accurate but should include language specific rules */
        BufferedReader br = new BufferedReader(new FileReader(file.toFile()));
        while (true) {
            try {
                String current = br.readLine();
                if (current == null) break;    // EOF handling
                StringTokenizer token = new StringTokenizer(current, " .,;:!?%*+=-—_()’'\"");    // Known delimiters
                for (int i = 0 ; i < token.countTokens() ; i++) {
                    String word = token.nextToken();
                    /* Handling numerical values */
                    try {
                        Double.parseDouble(word);
                        break;
                    }
                    catch (NumberFormatException exc) {}
                    if (word_list.contains(word.toLowerCase())) continue;    // Case insensitive
                    word_list.add(word.toLowerCase());
                }
            }
            catch (IOException exc) {
                break;
            }
        }
    }
    public void saveToFile() throws FileNotFoundException {
        DataOutputStream word_output = new DataOutputStream(
            new FileOutputStream("chapter20" + File.separator + file.getFileName().toString().replace("txt", "csv"))
        );
        try {
            for (String word : word_list) {
                word_output.writeBytes(word);
                word_output.writeChar(13);    // Pseudo-csv written as binary
            }
            word_output.close();
        }
        catch (IOException exc) {exc.printStackTrace();}
        System.out.println("Scanned " + file.getFileName().toString() + ": " + word_list.size() + " unique words");
    }
    private Path file;
    private ArrayList<String> word_list = new ArrayList<String>();    // HashMap could allow having a word count
}

public class WordCounter {
    /* Chapter 19 about Java applets that are deprecated will be skipped */
    public static void main(String [] args) throws IOException {
        Files.walkFileTree(Paths.get("chapter20"), new GetBooks());
        for (Path book : books) {
            WordCountProcessor wcp = new WordCountProcessor(book);
            wcp.process();
            wcp.saveToFile();
        }
    }
    public static class GetBooks extends SimpleFileVisitor<Path> {
        /* Populates a list of path to text files using the FileVisitor interface instead of File DirectoryStream */
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            PathMatcher match_text = FileSystems.getDefault().getPathMatcher("glob:**.txt");    // glob is not regex
            if (attr.isRegularFile() && match_text.matches(file)) {
                books.add(file);
            }
            return FileVisitResult.CONTINUE;    // Mandatory processing order
        }
    }
    private static ArrayList<Path> books = new ArrayList<Path>();
}
