package chapter16_17_18;

import java.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.io.*;
import java.nio.file.*;
import java.nio.charset.Charset;

class WordsLoader {
    public WordsLoader(String file_name) {
        char sep = File.separatorChar;    // OS compliance
        Path fp = new File(System.getProperty("user.dir") + sep + "assets" + sep + file_name).toPath();
        try {
            input_data = Files.readAllLines(fp, Charset.defaultCharset());
        }
        catch (IOException exc) {exc.printStackTrace();}
    }
    public String getRandomWord() {
        if (input_data == null) return "";
        return input_data.get(rnd.nextInt(input_data.size()));
    }
    private java.util.List<String> input_data;
    private Random rnd = new Random();
}

class WordPanel extends JPanel {
    public WordPanel(String word, Font word_font, WordsLoader wl) {
        this.wl = wl;
        this.word = word;
        this.word_font = word_font;
        next_letter = word.charAt(0);    // Only way to return a char from a String
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Font new_font = word_font.deriveFont(100f);    // Increasing the size (Font are somehow immutable for that)
        /* Deducing the boxing of the word */
        FontMetrics fm = g.getFontMetrics(new_font);
        int x = ((int) this.getSize().getWidth() - fm.stringWidth(word)) / 2;
        int y = ((int) this.getSize().getWidth() - fm.getHeight()) / 2;
        g.setFont(new_font);
        drawWord(g, x, y);
        g.drawString(String.valueOf(score), 20, 100);
    }
    public void drawWord(Graphics g, int x, int y) {
        g.drawString(word, x, y);
        String validated = word.substring(0, cursor);
        g.setColor(Color.GREEN);
        g.drawString(validated, x, y);
    }
    public void checkLetter(char l) {
        /**
         * Basic validator
         */
        if (l != next_letter) {
            score -= 1;
            this.repaint();
            return;
        }
        cursor++;
        if (cursor == word.length()) {
            score += word.length();
            cursor = 0;
            word = wl.getRandomWord();
            next_letter = word.charAt(0);
        }
        next_letter = word.charAt(cursor);
        this.repaint();
    }
    public void changeWord(String word) {
        this.word = word;
        this.repaint();
    }
    public void changeFont(Font font) {
        word_font = font;
        this.repaint();
    }
    WordsLoader wl;
    private int cursor, score = 0;
    private String word;
    private Font word_font;
    private char next_letter;
    private static final long serialVersionUID = 1L;
}

class FontRadio extends JRadioButtonMenuItem {
    public FontRadio(String name, boolean selected) {
        super(name.substring(0, 1).toUpperCase() + name.substring(1), selected);
    }
    public Font getWordFont() {
        return associated_font;
    }
    public void setWordFont(Font font) {
        associated_font = font;
    }
    private Font associated_font;
    private static final long serialVersionUID = 2L;
}

class TypingWindow extends JFrame implements ActionListener, KeyListener {
    public TypingWindow(WordsLoader wl) {
        this.wl = wl;
        setTitle("Typing game");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = getContentPane();

        /* Load custom fonts + menu */
        String [] font_names = {"bebas", "chillow", "instruction", "larabie"};    // Should load font in specific class
        JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);
        JMenu font_menu = new JMenu("Font");
        JMenu font_select = new JMenu("Select Font");
        ButtonGroup font_group = new ButtonGroup();
        try {
            ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            char sep = File.separatorChar;
            for (String font_name : font_names) {
                File ff = new File(
                    System.getProperty("user.dir") + sep + "assets" + sep + "fonts" + sep + font_name + ".ttf"
                );
                Font font = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(ff));
                ge.registerFont(font);
                FontRadio item;
                item = new FontRadio(font_name, font_name == "bebas");
                item.setWordFont(font);
                if (font_name == "bebas") default_font = font;
                item.addActionListener(this);
                font_group.add(item);
                font_select.add(item);
            }
        }
        catch (FontFormatException | IOException exc) {exc.printStackTrace();}
        font_menu.add(font_select);
        menu.add(font_menu);

        /* Setting the stage */
        word_panel = new WordPanel(this.wl.getRandomWord(), default_font, wl);
        content.add(word_panel);
        addKeyListener(this);
    }
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() instanceof FontRadio) {
            FontRadio radio_button = (FontRadio) evt.getSource();
            word_panel.changeFont(radio_button.getWordFont());
        } 
    }
    public void keyPressed(KeyEvent evt) {}
    public void keyReleased(KeyEvent evt) {}
    public void keyTyped(KeyEvent evt) {
        char c = evt.getKeyChar();
        if (c >= 65 && c <= 90) c = (char) ((int) c + 32);
        else if (c >= 97 && c <= 122) {}
        else return;
        word_panel.checkLetter(c);
    }
    private WordsLoader wl;
    private GraphicsEnvironment ge;
    private WordPanel word_panel;
    private Font default_font;
    private static final long serialVersionUID = 3L;
}

public class TypingGame {
    public static void main(String [] args) {
        TypingWindow gw = new TypingWindow(new WordsLoader("words_final.txt"));
        gw.setVisible(true);
    }
}
