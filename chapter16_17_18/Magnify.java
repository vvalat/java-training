package chapter16_17_18;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

import javax.imageio.ImageIO;
import java.awt.image.*;

class MoveWindow extends JFrame {
    public MoveWindow() {
        setTitle("Move objects");
        setSize(1550, 940);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = getContentPane();
        content.add(new MovePanel());
    }
    private static final long serialVersionUID = 1L;
}

class MovePanel extends JPanel implements MouseMotionListener {
    public MovePanel() {
        magx = (int) factor * margin; magy = (int) factor * margin;    // Coordinates on the big pictures
        addMouseMotionListener(this);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                mx = evt.getX();
                my = evt.getY();
                if (mx >= x && mx <= (x + w) && my >= y && my <= (y + h)) {    // Are we on it?
                    targeted = true;
                }
            }
            public void mouseReleased(MouseEvent evt) {targeted = false;}
        });
        try {
            waldo = ImageIO.read(new File("assets" + File.separator + "waldo.jpg"));
        }
        catch (IOException exc) {exc.printStackTrace();}
        waldo_mini = new ImageIcon("assets" + File.separator + "waldo_mini.jpg");
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(waldo_mini.getImage(), margin, margin, null);
        try {
            reduced = waldo.getSubimage(magx, magy, 500, 500);
        }
        catch (RasterFormatException exc) {}
        g.drawImage(reduced, 1020, margin, null);
        Graphics2D _g = (Graphics2D) g;    // Downcast allows setting stroke width (and some more)
        _g.setStroke(new BasicStroke(4f));
        _g.drawRect(x, y, w, h);
    }
    public void mouseMoved(MouseEvent evt) {}
    public void mouseDragged(MouseEvent evt) {
        if (targeted) {
            x = evt.getX() - mx + x;    // Compute new starting point for the rectangle
            y = evt.getY() - my + y;
            mx = evt.getX();
            my = evt.getY();
            magx = (int) ((x - margin) * factor);
            magy = (int) ((y - margin) * factor);
            this.repaint();
        }
    }
    private int x = 20, y = 20, h = 200, w = 200, mx, my, magx, magy, margin = 10;
    private float factor = 2.5f;    // In theory, should compute from both images specs but not the point here
    private boolean targeted;
    BufferedImage waldo, reduced;
    ImageIcon waldo_mini;
    private static final long serialVersionUID = 2L;
}

public class Magnify {
    public static void main(String [] args) {
        MoveWindow w = new MoveWindow();
        w.setVisible(true);
    }
}
