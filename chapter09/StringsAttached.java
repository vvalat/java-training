package chapter09;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class StringsAttached {
    /* There's no `in` operator to check a value against an array so we have to implement one */
    private static boolean match(String needle, String[] hay) {
        // First remark: there exist primitive type streams (Int, Double...)
        // Second remark: the Arrays.stream() does not accept char primitivze type, only String object
        // Third remark: this could have been done 'simply' in a for loop with `==` operator
        return Arrays.stream(hay).anyMatch(letter -> letter.equals(needle));    // More on this syntax later
    }

    enum AstrologicalSign {
        /**
         * Enumeration classes are very useful, especially for switch...cases syntax (see implementation below)
         */
        aries("\u2648"), taurus("\u2649"), gemini("\u264A"), cancer("\u264B"), leo("\u264C"), virgo("\u264D"),
        libra("\u264E"), scorpio("\u264F"), sagittarius("\u2650"), capricorn("\u2651"), aquarius("\u2652"),
        pisces("\u2653");

        private AstrologicalSign(String code) {
            this.code = code;
        }
        public String getSymbol() {
            return code;
        }
        private String code;
    }

    public static void main(String args[]) {
        String str = "Hello";
        str += " World!"; // Here the `str` variable is not modified, a temp String is created to replace it
        System.out.println(str);
        StringBuffer strb = new StringBuffer("This. Is. ");
        strb.append("Java!"); // `strb` is mutable, depending on the situation, this is more performant
        /* `StringBuffer` class also has lots of features (allocation capacity control...) as well as standard ones */
        System.out.println(strb);
        System.out.println(strb.reverse().charAt(0)); // Unlike python, there's no support for negative indexing
        System.out.println("Space @ " + str.indexOf("\s", 1)); // Searches for first space after first char
        /* It will return -1 if nothing is found. Note that `lastIndexOf()` searches starting from the end */

        char list[] = str.toCharArray(); // Using arrays for certain operations is more efficient
        String vowels[] = { "a", "e", "i", "o", "u", "y" };
        int count = 0;
        for (char letter : list)
            if (match(String.valueOf(letter), vowels)) count++; // `valueOf()`: primitive type to Object
        System.out.format("There's %d vowels in %s%n", count, str);

        String str2 = "Hello World!";
        System.out.println(str2.equals(str)); // Note that `==` only compare object references not values

        String str3 = "- 125";
        int val = Integer.parseInt(str3.replace(" ", "")); // `Type.parse...`: Object to primitive type
        System.out.println(val + 12);

        final List<AstrologicalSign> signs = Arrays.asList(AstrologicalSign.values());    // More on this syntax later
        Random rnd = new Random();
        AstrologicalSign rnd_sign = signs.get(rnd.nextInt(signs.size()));
        StringBuffer advice = new StringBuffer(rnd_sign + " (" + rnd_sign.getSymbol() + ")");
        switch (rnd_sign) {
            case aries:
                advice.append(" | Watch out for unhandled exceptions");
                break;
            case taurus:
                advice.append(" | Reorganize your classes");
                break;
            case gemini:
                advice.append(" | Too many overridden methods");
                break;
            case cancer:
                advice.append(" | Write comments");
                break;
            case leo:
                advice.append(" | Name explicitly your variables");
                break;
            case virgo:
                advice.append(" | Too many public things");
                break;
            case libra:
                advice.append(" | Split your code");
                break;
            case scorpio:
                advice.append(" | Review your pull requests");
                break;
            case sagittarius:
                advice.append(" | Commit often");
                break;
            case capricorn:
                advice.append(" | Be (thread) safe");
                break;
            case aquarius:
                advice.append(" | Be careful for return values");
                break;
            case pisces:
                advice.append(" | Write more tests");
                break;
        }
        System.out.println(advice);
    }
}
