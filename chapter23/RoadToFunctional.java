package chapter23;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

interface Repeater {public String repeat(int n, String text);}
/**
 * A lambda expression (arguments -> block expression) needs a functional interface that contains an abstract method
 * (only one but the interface can have static or default methods) which will be defined as the functional method.
 * There can be one, multiple or no arguments. Those are valid:
 *     n -> n + 2;    interface Sum {public int add(int n)}    used with (new Sum()).add(40)
 *     (a, b) -> a + b;    same interface    used with (new Sum()).add(30, 12)
 *     () -> 42;      interface FortyTwo {public int give();}     used with (new FortyTwo()).give()
 * Above, the block expressions can fit on one line but for more complex things curly brackets can be used.
 */

interface MathOps {public double do_math(int x);}

class Book {
    public Book(String title, double price) {
        this.title = title;
        this.price = price;
    }
    public String getTitle() {return title;}
    public double getPrice() {return price;}
    public String toString() {return title + " - €" + String.format("%.2f", price);}
    public String bundleTitle(Book other) {
        if (title == "" || title == null) return other.title;
        return title + " & " + other.title;
    }
    private String title;
    private double price;
}

public class RoadToFunctional {
    static void integer_predicate(int input, Predicate<Integer> predicate) {
        boolean result = predicate.test(Integer.valueOf(input));    // test is the functional method
        System.out.println("Result is " + result);
    }
    static double supplier(Supplier<Double> supplier) {
        return supplier.get();    // get is the functional method
    }
    static void consumer(int input, Consumer<Integer> consumer) {
        consumer.accept(input);    // accept is the functional method
    }
    static Integer compute(int x, IntUnaryOperator op) {
        return op.applyAsInt(x);    // applyAsInt is the functional method but it's a specific cast for apply()
    }
    static double square_root(int x) {return Math.pow((double) x, 0.5);}
    static void show_result(int x, MathOps comp) {
        System.out.println(comp.do_math(x));
    }
    static long fact(long n) {
        if (n == 0) return 1;
        return n * fact(n - 1);
    }
    public static void main(String[] args) {
        Repeater text_repeat = (n, text) -> {
            String out = "";
            for (int i = 0 ; i < n ; i++) out += (text + "\n");
            return out;
        };
        String multiple_yes = text_repeat.repeat(10, "yes");
        System.out.print(multiple_yes);

        /**
         * The java.util.function package contains functional interfaces with defined or generic types. Some examples
         * using the fact that a lambada expression can be used as an argument whose type is a functional interface.
         */
        integer_predicate(1, x -> x > 0);
        System.out.println("Some number: " + supplier(() -> Math.random()));    // takes no argument, returns T
        consumer(8, n -> integer_predicate(n, x -> x > 10));    // A consumer does the opposite through a void method
        System.out.println(compute(4, x -> 2*x*x + 5*x + 3));
        // Other possibilities include a lambda factory where an interface returns a functional interface where an
        // instance contains a nested lambda as defined by () -> (x -> ... )

        // A lambda can be replaced by a static method with the syntax Class::method (called a supplier in that context)
        show_result(256, RoadToFunctional::square_root);    // possible: super::, instanceOfClass::, this::, Class::new

        // A lambda can be used in a for...each loop:
        String [] table = {"1", "2", "3", "4"};
        List<String> numbers = new ArrayList<String>();    // List is the interface extending a generic collection
        numbers = Arrays.asList(table);    // converts an array to a list 
        numbers.forEach(elem -> {System.out.println(elem);});    // Streams are discussed just below

        /**
         * A lambda can also be used to generate a comparator that can be fed to a sort() method. Examples:
         * list.sort((a, b) -> ((Object) (a.getValue())).compareTo((Object) (b.getValue())))   if extending Comparable
         * list.sort(Comparator.comparing Class::method) where Class.method returns an Integer used for sorting
         * list.sort(Comparator.naturalOrder())   if the elements' class is implementing Comparable
         * list.sort(Comparator.comparing (Class::method).reversed())
         */

        /**
         * Streams consume a data structure such as a collection allowing different operations, mostly using lambda
         * expresions. There are generators, intermediate operations that return a stream and final operations that
         * close the stream. Examples :
         * generating: generate, iterate, Stream.of([]), Collection().stream()...
         * closing: forEach, forEachOrdered, count, reduce, collect, min/max, findAny/First, getAs, sum, average...
         * intermediate: filter, map, sorted, peek, distinct, limit...
         * Some examples:
         */
        Stream<String> number_stream = Stream.of(table);    // Equivalent to numbers.stream()
        number_stream.map(el -> Integer.parseInt(el)).filter(el -> el >= 2).forEach(el -> System.out.println(el));
        //              converts elements to Integer | keeps if gte than 2 | process each element and prints it
        Stream.generate(Math::random).limit(100).sorted(Comparator.naturalOrder()).filter(el -> el < 0.5).count();
        // The above generates a stream of 100 random numbers (without limit, it will infinitely loop), sorts them,
        // retain those above 0.5 and returns the number of elements in the stream
        long start = System.currentTimeMillis();
        LongStream.iterate(1, el -> el + 1).limit(10000).map(el -> fact(el)).sum();
        long end = System.currentTimeMillis();
        System.out.format("Without parallel computing: %d ms%n", end - start);
        start = System.currentTimeMillis();
        LongStream.iterate(1, el -> el + 1).limit(10000).parallel().map(el -> fact(el)).sum();
        end = System.currentTimeMillis();
        System.out.format("With parallel computing: %d ms%n", end - start);
        // The above is calculating the sum of the factorial of the first 10000 positive integers.
        // Note : for lesser limits, the parallelization overhead may indicate a grater processing time
        Book book_a = new Book("The Tides of Suffering", 10.5);
        book_a = Stream
            .of(book_a, new Book("The Winds of Misery", 12.8))
            .peek(elem -> System.out.println(elem))
            .reduce(
                new Book("", 0),
                (a,b) -> (new Book(a.bundleTitle(b), (a.getPrice() + b.getPrice() * 0.9)))
            );
        // The above reduces two Book instances (but could do more) returning a Book after displaying each element
        Stream<Book> book_stream = Stream.of(book_a, new Book("The Rains of Pain", 18.6));
        Map<String, Double> books = book_stream.distinct().collect(Collectors.toMap(Book::getTitle, Book::getPrice));
        System.out.println(books);
        // The above prints a collection made by a stream (the distinct() ensures unicity of keys, though
        // Collectors.groupingBy(Supplier) where each identical key generates a list of corresponding elements)
    }
}
