import os

if __name__ == "__main__":
    path = os.path.join(os.path.abspath(os.getcwd()), "assets", "words.txt")
    with open(path, "r") as file:
        data = [l for l in file.readlines() if len(l) > 4 and len(l) < 12]
        with open(path.replace(".", "_final."), "w") as w_file:
            w_file.writelines(data)
