package chapter13_14_15;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

import org.json.simple.*;
import org.json.simple.parser.*;

class TooltipCellRenderer extends DefaultTableCellRenderer {
    /**
     * Basic override to be able to define the tooltip width through the constructor
     */
    public TooltipCellRenderer(int width) {
        this.width = width;
    }
    public Component getTableCellRendererComponent(JTable t, Object v, boolean s, boolean f, int r, int c) {
        JLabel label = (JLabel) super.getTableCellRendererComponent(t, v, s, f, r, c);
        label.setToolTipText("<html><p width=\"" + width +"\">" + label.getText() + "</p></html>");
        return label;
    }
    private int width;
    private static final long serialVersionUID = 1L;
}

class NYTAPICaller {
    public NYTAPICaller(String query, boolean picks, String ordering, String key, int to, JFrame context) {
        /* Context is needed to display dialog boxes */
        this.context = context;
        timeout = to;
        String endpoint = picks ? "picks.json" : "all.json";
        try {
            url = new URL(API_PATH + endpoint + "?query=" + query + "&order=" + ordering + "&api-key=" + key);
        }
        catch (MalformedURLException exc) {}    // Should not happen but caught indirectly below, maybe not intuitive
    }
    public JSONArray getResults() {
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(timeout);
            int status = conn.getResponseCode();
            if (status == 200) {
                /* The following is a try-with statement that closes the stream reader w/o needing a finally block */
                try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                    StringBuilder sb = new StringBuilder();    // Output stream
                    String line;
                    while ((line = br.readLine()) != null) {    // Consuming the stream
                        sb.append(line += "\n");
                    }
                    String raw_data = sb.toString();
                    JSONParser jp = new JSONParser();
                    data = (JSONObject) jp.parse(raw_data);
                }
            }
            else {
                String msg = "API could not be reached (" + status + ")";
                if (status == 401) msg += ". Check API key settings.";
                JOptionPane.showMessageDialog(context, msg, "Connection error", JOptionPane.ERROR_MESSAGE);
                conn.disconnect();
                return null;
            }
            conn.disconnect();
        }
        catch (IOException | ParseException exc) {
            exc.printStackTrace();
            System.exit(1);    // Also could be better but at 300+ lines of code so...
        }
        return (JSONArray) data.get("results");
    }
    private JFrame context;
    private JSONObject data;
    private URL url;
    private int timeout;
    private final String API_PATH = "https://api.nytimes.com/svc/movies/v2/reviews/";
}

class ChangeColorAction extends AbstractAction {
    /**
     * Abstract actions allows sharing code between multiple listeners
     */
    public ChangeColorAction(String name, Color color, MovieReviewWindow context) {
        super(name);
        this.context = context;
        this.color = color;
    }
    public void actionPerformed(ActionEvent evt) {
        context.getContentPane().setBackground(color);
        context.changePanelColors(color);
    }
    private Color color;
    private MovieReviewWindow context;
    private static final long serialVersionUID = 2L;
}

class MovieReviewWindow extends JFrame implements ActionListener, FocusListener {
    /**
     * FocusListener allows us to detect focus change on a text field. There is also a DocumentListener that listens
     * to input directly made on that field (could be useful for autocomplete features but out of scope for now).
     */
    public MovieReviewWindow() {
        setTitle("New York Times Movie Reviews");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content = getContentPane();

        /* Basic structure */
        Box box = Box.createVerticalBox();    // Box layout
        option_panel = new JPanel();
        option_panel.setAlignmentX(Component.CENTER_ALIGNMENT);
        option_panel.setBorder(BorderFactory.createTitledBorder("Query options"));
        result_panel = new JPanel();
        result_panel.setPreferredSize(new Dimension(800, 400));
        Box inner_box = Box.createHorizontalBox();

        /* Menu */
        JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);
        JMenu file = new JMenu("File");
        file.setMnemonic('F');
        menu.add(file);
        JMenuItem export = new JMenuItem("Export");
        export.setMnemonic('X');
        export.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        JMenuItem quit = new JMenuItem("Quit");
        quit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
        quit.setMnemonic('Q');
        export.addActionListener(this); quit.addActionListener(this);
        file.add(export); file.add(quit);
        JMenu settings = new JMenu("Settings");
        settings.setMnemonic('S');
        menu.add(settings);
        JMenuItem api_key = new JMenuItem("Set API Key");
        api_key.setMnemonic('A');
        api_key.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
        JMenu timeout = new JMenu("Set timeout");
        ButtonGroup timeout_group = new ButtonGroup();
        JRadioButtonMenuItem _1000 = new JRadioButtonMenuItem("1 second", true);
        JRadioButtonMenuItem _3000 = new JRadioButtonMenuItem("3 seconds");
        JRadioButtonMenuItem _10000 = new JRadioButtonMenuItem("10 seconds");
        timeout_group.add(_1000); timeout_group.add(_3000); timeout_group.add(_10000);
        _1000.addActionListener(this); _3000.addActionListener(this); _10000.addActionListener(this);
        timeout.add(_1000); timeout.add(_3000); timeout.add(_10000);
        JMenuItem bg_color = new JMenuItem("Set background color");
        api_key.addActionListener(this); timeout.addActionListener(this); bg_color.addActionListener(this);
        settings.add(api_key); settings.add(timeout); settings.add(bg_color);

        /* Popup menu */
        JPopupMenu pop = new JPopupMenu();
        JMenu color = new JMenu("Background color");
        pop.add(color);
        /* These actions will implicitly be constructed as JMenuItem */
        ChangeColorAction change_white = new ChangeColorAction("White", Color.WHITE, this);
        ChangeColorAction change_lgray = new ChangeColorAction("Light gray", Color.LIGHT_GRAY, this);
        ChangeColorAction change_dgray = new ChangeColorAction("Dark gray", Color.DARK_GRAY, this);
        color.add(change_white); color.add(change_lgray); color.add(change_dgray);
        addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent evt) {
                if (evt.isPopupTrigger()) {
                    pop.show(evt.getComponent(), evt.getX(), evt.getY());
                }
            }
        });

        /* Toolbar */
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.add(change_white); toolbar.add(change_lgray); toolbar.add(change_dgray);
        content.add(toolbar, "South");

        /* Editor's picks */
        picks_checkbox = new JCheckBox("Select only Editor's picks", false);
        inner_box.add(picks_checkbox); 

        /* Query order */
        Box order_box = Box.createVerticalBox();
        JLabel order_label = new JLabel("Order results by:");
        ButtonGroup order_group = new ButtonGroup();
        by_title = new JRadioButton("title", true);
        by_review = new JRadioButton("date of the review");
        by_release = new JRadioButton("date of movie opening");
        order_group.add(by_title); order_group.add(by_review); order_group.add(by_release);
        order_box.add(order_label);
        order_box.add(by_title); order_box.add(by_review); order_box.add(by_release);
        inner_box.add(order_box);

        /* Query field and assembling stuff */
        query_string = new JTextField("Type the title of a movie (or words of it)", 30);
        query_string.addFocusListener(this);
        option_panel.add(query_string);
        option_panel.add(inner_box);
        box.add(option_panel);
        box.add(Box.createVerticalStrut(20));    // Add 20 px of vertical space
        start_query = new JButton("Search");
        start_query.setAlignmentX(Component.CENTER_ALIGNMENT);
        start_query.addActionListener(this);
        box.add(start_query);
        box.add(Box.createVerticalStrut(20));
        box.add(result_panel);
        content.add(box);
        pack();
        by_title.requestFocusInWindow();
    }
    public static String getYear(String date) {
        if (date == "" || date == null) return "";
        return date.substring(0, 4);
    }
    public JTable getMovieTable(JSONArray results) {    // Could be done in a separate class
        if (results == null) return new JTable();
        raw_json = (JSONArray) results.clone();
        Object [] result_array = results.toArray();
        Object [][] result_table = new Object [result_array.length][4];
        for (int i = 0; i < result_array.length; i++) {
            JSONObject line = (JSONObject) result_array[i];
            result_table[i][0] = line.get("display_title");
            result_table[i][1] = line.get("summary_short");    // Could escape HTML char codes but need extra lib
            result_table[i][2] = getYear((String) line.get("opening_date"));
            result_table[i][3] = (String) ((JSONObject) line.get("link")).get("url");
        }
        String [] headers = {"Title", "Summary", "Opening", "Link"};
        JTable movie_table = new JTable(result_table, headers);
        movie_table.getColumnModel().getColumn(0).setPreferredWidth(200);
        movie_table.getColumnModel().getColumn(0).setCellRenderer(new TooltipCellRenderer(100));
        movie_table.getColumnModel().getColumn(1).setPreferredWidth(500);
        movie_table.getColumnModel().getColumn(1).setCellRenderer(new TooltipCellRenderer(300));
        movie_table.getColumnModel().getColumn(2).setPreferredWidth(90);
        DefaultTableCellRenderer center_renderer = new DefaultTableCellRenderer();
        center_renderer.setHorizontalAlignment(JLabel.CENTER);
        movie_table.getColumnModel().getColumn(2).setCellRenderer(center_renderer);
        movie_table.getColumnModel().getColumn(3).setMinWidth(0);    // Hiding the URL column
        movie_table.getColumnModel().getColumn(3).setMaxWidth(0);
        movie_table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                int sel_row = movie_table.rowAtPoint(new Point (evt.getX(), evt.getY()));
                try {
                    Desktop.getDesktop().browse(new URI((String) movie_table.getValueAt(sel_row, 3)));
                }
                catch (IOException | URISyntaxException exc) {}
            }
        });
        movie_table.setAutoCreateRowSorter(true);
        return movie_table;
    }
    public void actionPerformed(ActionEvent evt) {
        switch (evt.getActionCommand()) {    // Could be refactored by having multiple action listeners
            case "Search":
                result_panel.removeAll();
                String order = "by-";
                order += by_title.isSelected() ? "title" :
                    (by_review.isSelected() ? "publication-date" : "opening-date");
                NYTAPICaller nyt_call = new NYTAPICaller(
                    query_string.getText(), picks_checkbox.isSelected(), order, key, timeout, this
                );
                /* Strangely enough, embedding in a scroll pane is needed to display headers */
                JScrollPane result_container = new JScrollPane(getMovieTable(nyt_call.getResults()));
                result_container.setPreferredSize(new Dimension(600, 277));
                result_panel.add(result_container);
                result_panel.revalidate(); result_panel.repaint();
                break;

            case "Export":
                if (raw_json == null) {
                    JOptionPane.showMessageDialog(this, "Nothing to export");
                    break;
                }
                File file = new File(query_string.getText() + ".json");
                file.setWritable(true); file.setReadable(true);
                try (FileWriter fw = new FileWriter(file)) {
                    fw.write(raw_json.toJSONString());
                    fw.flush();
                    JOptionPane.showMessageDialog(this, "File has been saved to " + file.getAbsolutePath());
                }
                catch (IOException exc) {
                    exc.printStackTrace();
                    JOptionPane.showMessageDialog(this, "Could not export", "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;

            case "Quit": System.exit(0); break;

            case "Set API Key":
                key = JOptionPane.showInputDialog(this, "Input your API key");

            case "1 second": timeout = 1000; break;
            case "3 seconds": timeout = 3000; break;
            case "10 seconds": timeout = 10000; break;

            default: break;
        }
    }
    public void focusLost(FocusEvent evt) {}    // No action needed
    public void focusGained(FocusEvent evt) {
        if (init) {
            query_string.setText("");
            init = false;
        }
    }
    public void changePanelColors(Color color) {
        option_panel.setBackground(color); repaint();
        result_panel.setBackground(color); repaint();
    }
    private JPanel option_panel;
    private JPanel result_panel;
    private JTextField query_string;
    private JButton start_query;
    private JCheckBox picks_checkbox;
    private JRadioButton by_title, by_review, by_release;
    private String key;
    private int timeout = 1000;
    private JSONArray raw_json;
    private boolean init = true;
    private static final long serialVersionUID = 3L;
}

public class NYTMovieReviews {
    public static void main(String [] args) {
        MovieReviewWindow main_window = new MovieReviewWindow();
        main_window.setVisible(true);
    }
}
