package chapter21_22;

class PointInSpace<T extends Number>{
    /**
     * The notation <T> (conventional) indicates that type is set at instantiation. Internally, this has no meaning.
     * As byte codes, it is simply cast as an Object. In that regard, T doesn't exist (it is not a class, thus cannot
     * be instantiated).
     * Extending a generic type (here to a Number) restricts it. We can also use an interface (Comparable, Cloneable)
     * or several ones at the same time -> ...extends Comparable & Serializable
     * Note that an array of generic type class is not possible (That's what java.util.<collections> are for)
     * Note also that a PointInSpace<Integer> is the same as a PointInSpace<Float> unless doing some introspection.
     */
    public PointInSpace(T x, T y, T z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public String toString() {
        return "Point @ " + x + ":" + y + ":" + z;
    }
    private T x, y, z;
    static String reference = "3D point";    // This will be shared through all instances, regardless of type used 
}

class Descriptor {
    public Descriptor(String name) {
        this.name = name;
    }
    public String toString() {
        return name;
    }
    private String name;
}

class FullDescriptor extends Descriptor {
    public FullDescriptor(String name, String verbose) {
        super(name);
        this.verbose = verbose;
    }
    public String toString() {
        return super.toString() + " // " + verbose;
    }
    private String verbose;
}

class NamedPointInSpace<T extends Number, U extends Descriptor> extends PointInSpace<T> {
    /**
     * It is possible as illustrated to define several different generic types with or without restrictions.
     * Inheritance with generic types is rather complex but here are some of the possibilities:
     *     - adding a generic type (as it is the case here)
     *     - adding restrictions or specifying a type
     *     - creating a generic class from a standard one
     * Note that even if B is a child of A, C<B> is not a child of C<A>
     */
    public NamedPointInSpace(T x, T y, T z, U desc) {
        super(x, y, z);
        description = desc;
    }
    public String toString() {
        return description + " -> " + super.toString();
    }
    Descriptor description;
}

public class GenericsThings {
    // There's also generic methods usually returning the generic type. There can also multiple arguments.
    static <T> T fifty_fifty(T [] input) {
        if (input.length != 2) return null;
        return input[(int) (Math.random() * 100) % 2];  
    }
    public static void main(String [] args) {
        PointInSpace<Integer> a = new PointInSpace<Integer>(1, 12, 23);
        PointInSpace<Float> b = new PointInSpace<Float>(2.5f, 18.3f, 54.89f);
        /**
         * Note that we used Integer/Float and not int/float so the above calls are implicitly equivalent to
         *     new PointInSpace<Integer>(new Integer(1), new Integer(12), new Integer(23));
         *     new PointInSpace<Float>(new Float(2.5f), new Float(18.3f), new Float(54.89f));
         */

        // new PointInSpace<String>("12", "54", "89");  This is not accepted

        new NamedPointInSpace<Integer, Descriptor>(764, -15, 847, new FullDescriptor("A", "The point called A"));

        String [] text = {"heads", "tails"};
        System.out.println(fifty_fifty(text));
        Integer [] digit = {0, 1};
        System.out.println(fifty_fifty(digit));

        // It is possible to use polymorhpism with a wild card
        PointInSpace<?> point;
        point = a;
        point = b;
        System.out.println(point);
    }
}
