package chapter21_22;

import java.util.*;
import java.lang.Exception;
import java.time.LocalDate;

/**
 * There are two kinds of iterators: Iterator (monodirectional) and ListIterator (bidirectional). The latter,
 * in addition to next/hasNext, has access to previous/hasPrevious methods and the add method for inserts.
 * Collections have specific management methods to handle two collections (c1.addAll(c2) for union,
 * c1.retainAll(c2) for intersection, c1.removeAll(c2) for the complement of c2)
 * Note : collections also have add/remove capabilities based upon index/hash but they should NEVER be used
 * while iterating (use Iterator's equivalent). They return booleans depending on success, no exception is raised.
 * The contain() method allows search by values (recommended for HashSet (O(1)) or TreeSet (O(Log N)))
 * The size() methods returns an int of the number of elements
 * Note a for...each implicitly use a monodirectional iterator and forbids manipulation of members (add/remove)
 */

class MyLinkedList<T> extends LinkedList<T> {
    /**
     * Each member (or node) is linked to the previous and the next one (except for first and last) through pointers
     * to the memory space of that member (the current position is also a pointer to the value of the member).
     * Access is O(N) (since the whole list could be iterated over to find the wanted value)
     * Insert/Delete is O(1) at current position, O(N) otherwise (different position or collection method)
     * Iterator: ListIterator
     * Methods :
     *     - add (insert at current position, shift next members by 1, increase size by 1)
     *     - set (change value at current position)
     *     - next, previous (returns value after changing position)
     *     - remove (delete the member, reduce size by 1 but does not change position)
     */
    public String toString() {
        ListIterator<T> iter = this.listIterator();
        String str = "";
        while (iter.hasNext()) {
            str += iter.next().toString();
            str += " ";
        }
        return str;
    }
    private static final long serialVersionUID = 1L;
}

class MyArrayList<T> extends ArrayList<T> {
    /**
     * Each member is referenced in a contiguous memory space (formerly called Vector)
     * Access is O(1) as long as the size does not exceed capacity (in that case it's O(N))
     * Insert/Delete is O(1) at the end of the vector, O(N-i) when specifying an index i for shifting cost
     * Iterator: ListIterator though it is mostly used with get/set
     * Note: The capacity is defined by the constructor (e.g. 10) but it can be adjusted with .ensureCapacity(int)
     * or trimmed down with .trimToSize() when writing is done to free up memory space.
     *     - add (insert at selected position or at the end of the vector, shift next members by 1, increase size by 1)
     *     - set (change value at selected position)
     *     - next, previous (returns value after changing position)
     *     - remove (consume the value, shift next members by -1, reduce size by 1)
     */
    private static final long serialVersionUID = 2L;
}

class Hero {
    public Hero(String name, int age, int country_code) throws Exception {
        if (age < 18 || age > 70) {
            throw new Exception("No minors or seniors in the league");
        }
        if (name.length() < 3) {
            throw new Exception("No less than three letters for a name");
        }
        this.name = name;
        this.age = age;
        this.country_code = country_code;
    }
    public String toString() {
        return "Hello, my name is " + name + ", " + age;
    }
    public int hashCode() {
        /* Simple but should provide good distribution */
        int hash = age + country_code;
        for (char c : name.toLowerCase().substring(0, 3).toCharArray()) hash += (int) c;
        return hash;
    }
    public boolean equals(Object h) {
        Hero hero = (Hero) h;    // Convention so that equality check is not bound to type
        return hero.name.toLowerCase().equals(this.name.toLowerCase())    // Got tricked with '==' operator not working
            && hero.age == this.age
            && hero.country_code == this.country_code;
    }
    String name;
    int age;
    int country_code;
}

class TreeHero extends Hero implements Comparable<Hero> {
    public TreeHero(String name, int age, int country_code) throws Exception {
        super(name, age, country_code);
    }
    public String toString() {
        return "~" + name + ", " + age + "~";
    }
    public int compareTo(Hero hero) {
        /* The logic for equality should be consistent with the one above. */
        if (hero.age > this.age) return -1;
        if (hero.age < this.age) return 1;
            // hero.age == this.age
            if (hero.country_code > this.country_code) return -1;
            if (hero.country_code < this.country_code) return 1;
            // hero.age == this.age && hero.country_code == this.country_code
            int name_comp = hero.name.toLowerCase().compareTo(this.name.toLowerCase());
                if (name_comp < 0) return -1;
                if (name_comp > 0) return 1;
                    // hero.age == this.age && hero.country_code == this.country_code && hero.name == this.name
                    else return 0;
    }
}

class MyHashSet<T> extends HashSet<T> {
    /**
     * Ultimately, this is an array of linked list. Each array element is called a bucket and contains a linked list of
     * pointers to each member. The main advantage is that access, insertion, removal and use of contains() are O(1).
     * The concept relies on a hash table where each value goes through a hash function that returns an integer that
     * will be used to construct the index of the array such as idx = hash % N, where N is the number of buckets.
     * Note : N is initialized for the number of elements (Ne) as inputs and will be readjusted so that Ne <= N * 0.75.
     * For instance: if N is 12, Ne <= 9. If a 10th element is added, N is doubled (24) and every index is rehashed.
     * The quality of the implementation of the hashing function (hashCode() in the member class) relies on spreading
     * members into buckets as evenly as possible (i.e. reduce the collision probability as much as possible).
     * Furthermore, the equals() method needs to be overriden to assert equality condition between two members since
     * add() will first hash the object then look for the corresponding bucket than iterate over each element to assert
     * equality. Since a set cannot contain two similar elements, add() will then return true if created or false if it
     * already exists.
     */
    private static final long serialVersionUID = 3L;
}

class MyTreeSet<T> extends TreeSet<T> {
    /**
     * TreeSet relies on a binary tree that is not as optimized as a HashSet (O(Log N) instead of O(1)) but is already
     * in sorted order by design. It can be represented as an inverted tree where each node (or leaf) has up to two
     * branches. In order to search or insert, it relies on the compareTo() (equals() is not used) method that will
     * return for leaf.compareTo(input):
     *     -1 if leaf < input (go right)
     *      0 if leaf == input (stop)    <- this should have the same logic as equals()
     *      1 if leaf > input (go left)
     */
    private static final long serialVersionUID = 3L;
}

class Action implements Comparable<Action> {
    public Action(String action_name, LocalDate run_at) {
        name = action_name;
        run = run_at;
    }
    public String toString() {
        return name + " scheduled for " + run;
    }
    public int compareTo(Action a) {
        return this.run.compareTo(a.run);
    }
    private String name;
    private LocalDate run;
}

class MyQueue<T extends Comparable<T>> extends PriorityQueue<T> {    // Priority is defined by comparison for T
    /**
     * Queues and double-ended queues (deques) are interfaces that allows the data structure to be used for
     * populating and consuming objects in a running thread.
     * Classes implementing Queue: LinkedList, PriorityQueue + ArrayDeque (ArrayList with Deque)
     * Inserting: offer
     * Reading: poll (consuming), peek (non-consuming)
     * Note: for Deque, methods are offerFist and offerLast, peekFirst and peeklast and so on...
     * Note: add, get and remove can be used but they raise exception with full/empty queues instead of a boolean value
     */
    private static final long serialVersionUID = 4L;
}

class MyDict<K,V> extends HashMap<K,V> {
    /**
     * A hash map is an associative array (key, value) where the key can be an Object as long as keys can be compared
     * or sorted. The comparison follows the same principle as the HashSet (hashcode + equals) or TreeSet (compareTo).
     * The syntax use generic types for keys and values.
     * Performance is similar to corresponding sets.
     * Insert with put(K, V). Access with get(K).
     * For parsing, the keySet() method returns a Set of the keys, the values() method returns a Collection of the
     * values. The distinction is that keys are meant to be unique, but not the value so Collection retains duplicates.
     */
    public Iterator<Map.Entry<K,V>> getIterator() {
        /**
         * Maps are not meant to be iterated over as opposed to the main use of get(). However, something like this can
         * be done.
         */
        Set<Map.Entry<K,V>> content = this.entrySet();    // force the current content in a two-dimensional set
        return content.iterator();
    }
    private static final long serialVersionUID = 5L;
}

public class Collections {
    public static void main(String [] args) throws Exception {
        MyLinkedList<String> mll = new MyLinkedList<String>();
        mll.add("Hello");
        mll.add("beautiful");
        mll.add("world");
        ListIterator<String> iter = mll.listIterator();
        iter.next();    // At pos 0
        iter.set("Hi");
        iter.next();    // At pos 1
        iter.remove();
        iter.next();    // Change position to 2
        iter.add("!");  // Inserts at current pos (EOL).
        // Note: the last two instructions coul have been replaced by mll.addLast("!") but not while iterating
        System.out.println(mll);

        MyArrayList<Integer> mal = new MyArrayList<Integer>();
        mal.ensureCapacity(20);
        for (int i = 0 ; i < 20 ; i++) mal.add(Integer.valueOf(i));
        mal.remove(0);    // 0 is removed from the list
        mal.add(20);
        MyArrayList<Integer> odd = new MyArrayList<Integer>();
        for (int i = 0 ; i < 20 ; i= i + 2) odd.add(mal.get(i));    // Get odd numbered values
        mal.removeAll(odd);    // Remains even numbered values
        mal.trimToSize();    // For memory's sake

        Hero [] heroes = {
            new Hero("Bionic Man", 30, 57), new Hero("Bionic Girl", 24, 57), new Hero("bionic Man", 30, 57)
        };
        MyHashSet<Hero> mhs = new MyHashSet<Hero>();
        boolean [] results = new boolean [3];
        for (int i = 0 ; i < 3 ; i++) results[i] = mhs.add(heroes[i]);
        for (boolean b : results) System.out.println(b);

        TreeHero carson = new TreeHero("Rachel Carson", 57, 840);
        TreeHero bishop = new TreeHero("Barry Bishop", 62, 840);
        TreeHero hughes = new TreeHero("Nicholas Hughes", 47, 826);
        TreeHero lovelock = new TreeHero("James Lovelock", 47, 826);
        MyTreeSet<TreeHero> mts = new MyTreeSet<TreeHero>();
        mts.add(carson);
        mts.add(bishop);
        mts.add(hughes);
        mts.add(lovelock);
        System.out.println(mts);

        Action [] actions = {
            new Action("Soon", LocalDate.now().plusWeeks(1)),
            new Action("Later", LocalDate.now().plusMonths(2)),
            new Action("Urgent", LocalDate.now())
        };
        MyQueue<Action> mq = new MyQueue<Action>();
        for (Action a : actions) mq.offer(a);
        while (!mq.isEmpty()) System.out.println(mq.poll());

        MyDict<TreeHero,Action> planning = new MyDict<TreeHero,Action>();
        planning.put(carson, actions[1]);
        planning.put(bishop, actions[0]);
        planning.put(lovelock, actions[2]);
        Iterator<Map.Entry<TreeHero,Action>> planning_iter = planning.getIterator();
        while (planning_iter.hasNext()) {    // Semantically equivalent to a for...loop with keySet() and get()
            Map.Entry<TreeHero,Action> current = (Map.Entry<TreeHero,Action>) planning_iter.next();
            System.out.println(current.getKey() + " -> " + current.getValue());
        }
    }
}
