package chapter06;

public class ClassesGalore {
    /** Basic exemple of implementation to use possible syntax and recommended use
     * Note : There can be multiple classes in a single source file (as shown here),
     * but only one can be public and the main() method should be in that one. Other
     * possibilities would have included an Ingredient.java + Recipe.java sources that
     * defined public classes but there a lot of options depending on coding guidelines
     * and organization...
     */
    public static void main(String args[]) {
        Ingredient egg = new Ingredient("egg", 1.0f, "piece");
        Ingredient sugar = new Ingredient("sugar", 1.5f, "tsp");
        Ingredient ingredients[] = {egg, sugar};
        Recipe recipe = new Recipe(ingredients, 2);
        System.out.println(recipe.generateListOfIngredients());
        Ingredient.ingredientsCount();
        IngredientVariant new_egg = new IngredientVariant("egg", 5);
        new_egg.describe();
        IngredientVariant new_sugar = new IngredientVariant("sugar", 3.5f, "tsp");
        new_sugar.describe();
    }
}

class Ingredient {    // Without access attribute `public`, it can only be accessed within this package
    public Ingredient(String name, float quantity, String quantifier) {
        /* The following constructor could be omitted, require no arguments or be implemented through
         * a `initialise()` method as long as the logic is implemented one way or another
         * Also it is here public for obvious reasons but since there can be more than one, it coud be private as
         * long as there is one public to allow instantiation
         */
        this.name = name;                // The class ref is not mandatory unless we want the keep the signature names
        this.quantity = quantity;        // As such defining `private float qt` below and `qt = quantity` was possible
        this.quantifier = quantifier;
        count++;
    }
    public String describe() {    // public because should be called with object but no sense making it static
        /* Could be refined as to display human readable equivalent of floats through fractions and also
         * handle specific plurals
         */
        String quantified;
        String _name = name;         // We can no longer modify name so we use a temp variable
        if (quantifier == "piece") {
            quantified = " ";
            if (quantity > 1) _name = name + "s";
        }
        else quantified = " " + quantifier + " of ";
        return quantity + quantified + _name;
    }
    public void increase(int factor) {
        /*  Allows the ingredient to be multiplied by a factor to account for bigger quantities
         *  Note: Only public methods should allow modifications of private properties (encapsulation),
         *  such as getter/setter
         */
        this.quantity *= factor;    // The class reference is not mandatory but helps reading
    }
    public Ingredient copy() {
        /* Example of a cloning method that returns a copy of the instance to prevent messing with references
         * Note: if one of the property was itself an object, a `deepcopy()` that will clone the object should
         * be required (otherwise the same inner object could be modified by two different outer objects)  
         */
        return new Ingredient(name, quantity, quantifier);    // equivalent to `(this.name,...)`, depends on ambiguity
    }
    private final String name;        // We don't want the name to be altered
    private String quantifier;        /*  We protect those two properties from outside interference */
    private float quantity;           /*  though we can still modify them internally as shown above */
    /**
     * IMPORTANT : Declaration should be done outside of constructor because Java instantiation order:
     * 1 - Every property is initialized with default values (0, null, false...)
     * 2 - Explicit property declaration (i.e. `private String foo = "bar"`) is processed
     * 3 - The constructor is processed
     * As such the constructor is overloading declared and initialized properties
     * By convention, this is usually done at the end of the class definition
     */
    private static int count;     // No need here, but could have been set within a `static {...}` initialization block 
    public static void ingredientsCount() {
        /** Example of a class method. Those are mostly used for metainformation about the class or generic methods
         *  that do not require anything (or class variables). Non static private variables cannot be accessed.
         */
        System.out.println("Ingredients added: " + count);
    }
}

class Recipe {    // Same remark regarding access attribute as above
    public Recipe(Ingredient[] list, int nb_people) {
        ingredients = list;
        for (Ingredient ingredient : ingredients) ingredient.increase(nb_people);    // for...each syntax
    }
    public String generateListOfIngredients() {
        String output = "";
        for (int i = 0 ; i < ingredients.length ; i++) output += ingredients[i].describe() + ", ";
        return output;
    }
    private Ingredient[] ingredients;
}

class IngredientVariant {
    /**
     * Overloading in Java means that we can have several constructors, methods with the same name...
     * It should be used with caution because the compiler "decides" which implementation to use
     * according to the arguments that were provided. We should remember that it also automatically
     * converts so ambiguity may arise (int converted to long, etc...)
     * Though it really helps as it allows to rewrite the Ingredient class more cleanly and useful:
     */
    public IngredientVariant(String name, int quantity) {
        this.quantity = quantity;
        this.name = (quantity > 1) ? name + "s" : name;
        this.quantifier = "";
        is_int = true;
        this.cook = new CookingMethod(150, "oven");  // Should be set with arguments but irrelevant for the example
        System.out.println("Cook me at " + this.cook.temperature + " in " + this.cook.type);
    }
    public IngredientVariant(String name, float quantity, String quantifier) {
        this.name = name;
        this.quantity = quantity;
        this.quantifier = quantifier;
        is_int = false;
    }
    public void describe() {
        String quantified = (quantifier != "") ? " " + quantifier + " of " : " ";
        if (is_int) System.out.format("%d%s%s%n", (int) quantity, quantified, name);
        else System.out.format("%.1f%s%s%n", quantity, quantified, name);
    }
    class CookingMethod {
        /** Internal classes can be defined that way but both the internal and the external have access to each other
         *  properties and methods (including private access). Basic use: the external class is linked to the internal
         *  one through specific "has a" relation (a Circle has a Center, a Painting has a Technique...)
         */
        public CookingMethod(int temp, String method) {
            temperature = temp;
            type = method;
            System.out.println("I relate to " + name + " ingredient");
        }
        private int temperature;
        private String type;
    }
    private final String name;
    private String quantifier;
    private float quantity;     // Note: quantity cannot be of two different types, so we choose the best suited
    private boolean is_int;     // Though we can do something to make the display better
    private CookingMethod cook;
}