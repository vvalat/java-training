package chapter01_02;

import java.util.Scanner;

public class HumbleBeginnings {
    /** Basic example to include syntax for the first chapter examples of the guidebook
     *   Combines declaration, if / for, constants, input read, comments, print...
     */
    public static void main (String args[]) {
        Scanner input = new Scanner (System.in);
        System.out.println("Enter an integer to display product table :");
        int term = input.nextInt();
        input.close();
        if (term == 0 || term > 10)
            /* No need to create a block for single-line instructions */
            System.out.println("Won't do");
        else
        {
            final int START, END ;  // multiple declaration is allowed
            START = 1;
            END = 10;
            int i;
            for (i = START ; i <= END ; i++)
                {
                    System.out.println(term + " x " + i + " = " + (term*i));
                }
        }
    }
}