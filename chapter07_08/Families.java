package chapter07_08;

abstract class Toy {
    /** Abstract classes cannot be instantiated. They need to be extended. It can contain either standard methods
     *  that will be available to classes extending it or abstract definitions that will need to be defined by them.
     *  Notes:
     *      - any class having one abstract method will be implicitly an abstract class
     *      - an abstract class can be derived from a standard one or another abstract class
     *      - if an implementation of an abstract method uses arguments, they must be in the abstract signature
     *      - class extended from abstract ones don't need to redefine abstract methods (but it will be also abstract)
     */
    public Toy(String [] categories) {
        this.categories = categories;
    }
    public abstract String describe();    // Abstract methods must be public (to be redefined, it must be accessed)
    public String showCategories() {
        return String.join(" ", this.categories);
    }
    private String [] categories;
}

class Ball extends Toy implements Product{
    public Ball(String color, float price) {
        super(categories);    // Must be the first instruction in the constructor
        this.color = color;
        this.price = price;
    }
    public String describe() { // Must be public (children cannot reduce the visibility, only increase it)
        return "I'm a " + color + " " + verbose_name + " and I cost " + displayPrice();
    }
    public String displayPrice() {
        return price + " rolling " + currency;
    }
    public double priceWithTaxes() {
        return (double) (price * (1 + (tax / 100)));
    }
    public Ball clone() {
        return new Ball(color, price);
    }
    private String color;
    private float price;
    /* It's legal to increase visibility below (to illustrate the possibility, it's not recommended in order to
     * maintain encapsulation). Also needs to be static to be used in the constructor for super() */
    public static String[] categories = { "outdoor", "indoor" };
    private static String verbose_name = "ball";
}

class SoftBall extends Ball {
    /** Basic children class. It does not have access to private members of its parent, like `verbose_name`.
     *  It can add new properties (or class members) but cannot redefine them. In addition, the child class
     *  must fully satisfy the parent's construtor.
     */
    public SoftBall(double sf) {
        super("yellow", 8f);      // Typically, the constructor would use values from instantiation there
        softness_factor = sf;
    }
    public String describe() {
        return super.describe() + ", " + isSoft();    // without `super`, it will recursively call itself
    }
    public String isSoft() {
        return (softness_factor > 5) ? "I'm soft" : "I'm not so soft";
    }
    private static double softness_factor;
}

class CustomSoftBall extends SoftBall {
    public CustomSoftBall() {
        super(softness_factor);
    }
    public String isSoft(boolean arg) {
        return arg ? "yes" : "no";
    }
    public String isSoft(double comp) {
        return (softness_factor > comp) ? "I'm soft" : "I'm not so soft";
    }
    /** The two methods above are legal, you can override both the parent method and the child method.
     *  As previously, the compiler will try to match the signature given from the current class up to
     *  the highest parent. The return type sould be coherent.
     *  Warning : with automatic conversions and ellipsis i.e. `public String isSoft(int... args)`,
     *  the resolution can get complex
     */
    private static double softness_factor = 10;
}

public class Families {
    public static void main(String args []) {
        Ball ball = new Ball("red", 11.5f);
        System.out.println(ball.priceWithTaxes());
        SoftBall soft_ball = new SoftBall(2.5);
        System.out.println(soft_ball.describe());
        CustomSoftBall custom = new CustomSoftBall();
        System.out.println(custom.isSoft() + " | " + custom.isSoft(true) + " | " + custom.isSoft(15));

        /* Polymorphism illustration: a class can be assimilated to its parent being simil ar yet different */
        SoftBall poly;
        poly = new SoftBall(4);         // Default case
        poly = new CustomSoftBall();    // Allowed because a CustomSoftBall "is a" SoftBall
        /* `poly = new Ball(...);` would have been illegal, polymorphism works only as a bottom-up approach */
        Ball [] list = {poly, new Ball("blue", 15f)};    // This is allowed as an array of `Ball`
        for (int i = 0 ; i < list.length ; i++) System.out.println(list[i].describe());
        /* A SoftBall array with a Ball wouldn't have been possible as well as calling methods undefined for Ball */
        /* This also means there can be an automatic conversion (in arguments or assignation) from a child to a parent
         * and you can cast an object from a parent to a child class: `Ball b; b = new Ball(...); b = (SoftBall) b`
         */

         /* As every class inherits from Object, every child has access to generic methods */
         System.out.println(poly.equals(ball));
         /* This only compares refs, we need to override equals() to implement object comparison logic */
         System.out.println(poly.toString());
         /* By default prints `package.ClassName@HexAddress`, needs to be overridden for custom display */
    }
}