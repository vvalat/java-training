package chapter07_08;

public interface Product {
    /** Interfaces, in the same way as abstract classes, cannot be instantiated but should be implemented.
     *  Here, every method needs to be redefined (with the exception of `default` methods) and constants
     *  can be defined (but only as public and static+final).
     *  The main advantage is that a class can implement multiple interfaces (when it can extend only one since
     *  mutiple inheritance is not allowed). In that case, every combined methods needs to be redefined.
     *  Also interfaces can extend other interfaces with the same consequences as redifinitions go...
     */
    double priceWithTaxes();             // redefining is mandatory
    default String displayPrice() {      // redefining is not mandatory but would be a good idea to do so...
        return "nothing...";
    }
    static final String currency = "€";
    static final float tax = 6f;
}

class Box implements Cloneable {
    /** The Cloneable interface allow access to the clone() method as a shallow copy.
     *  It is recommended as language specification that the method should be overriden (implementing deep copy...)
     */
    public Box(String ref, Ball content, int weight) {
        reference = ref;
        contains = content;
        this.weight = weight;    // As before, the int is converted automatically into an Integer object
    }
    final String getReference() {    // Here `final` means that the method cannot be overridden by its children
        return reference;
    }
    public Box clone() {
        return new Box(reference, contains.clone(), weight.intValue());    // or (int) weight or just weight...
    }
    protected static String reference;    // The `protected` access only allow children and package classes to it
    private Ball contains;
    private Integer weight = 10;
    /* We wrap the primitive type `int` into an object (to be used in a collection, or passed as ref for instance) */
}

final class Container extends Box {    // A `final` class cannot be inherited from
    public Container(String ref, Ball content, int weight) {
        super(ref, content, weight);
    }
}
