package chapter24_25;

import java.lang.reflect.*;    // this package contains everything for introspection
import java.lang.annotation.*;    // this package contains everything for annotations (duh)
import java.time.*;
import java.time.format.*;
import java.time.temporal.ChronoUnit;

@SuppressWarnings("unused")    // This annotation allows IDE/compilation warnings to be bypassed (not recommended)
class Dummy {
    public Dummy() {}
    public void ImPublic() {}
    private String ImPrivate() {return "Hello";}
    public int public_number;
    private boolean private_boolean;
}

@Retention(RetentionPolicy.RUNTIME)    // This is mandatory so that the compiler retains annotation information
@Target({ElementType.TYPE})    // only allow class/enum to use annotation (constants include METHOD, FIELD...)
@Inherited    // allow children of annotated class to have annotation applied to them
@interface MyAnnotation {
    int [] lottery_numbers();    // parameters follow the method syntax
    String message() default "Hello";    // default allows not declaring the value
}

@MyAnnotation(lottery_numbers = {45, 28, 36, 12, 56})
class Lotto {}

public class TimeIntrospection {
    public static void main(String [] args) {
        Dummy dummy = new Dummy();
        Class<? extends Dummy> cls = dummy.getClass();
        // Note that getClass does not return Class<Dummy> in order to get Dummy children
        cls = Dummy.class;    // This is equivalent
        Field [] fields = cls.getDeclaredFields();
        // Note 1: getDeclaredField(name) allows querying a field by name or returning a NoSuchFieldException
        // Note 2: getFields returns public fields alongside parent fields
        for (Field field : fields) {
            System.out.println(field.toString() + " - " + Modifier.isPublic(field.getModifiers()));
            // getModifiers return an integer. Also exist: getType, getName...
        }
        Method [] methods = cls.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method.toString() + " - " + method.getReturnType());
            // Also exist: getParameterTypes, getExceptionType, getModifiers
        }
        Constructor<?> construct = cls.getDeclaredConstructors()[0];
        System.out.println(construct.getDeclaringClass());
        // Artificial use here but could be useful to discriminate several constructor with getParameterCount/Types

        for (Annotation annotation : Lotto.class.getAnnotations()) {
            MyAnnotation my_annotation = (MyAnnotation) annotation;
            for (int nb : my_annotation.lottery_numbers()) {System.out.println(nb);}
        }

        // Time has already been manipulated before but here are some examples that could be useful
        System.out.println(Duration.between(Instant.now(), Instant.now()));    // More readable than using '-' operator
        LocalDate dob = LocalDate.of(1980, 1, 28);
        System.out.println(
            DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(dob)
            + " was a "
            + dob.getDayOfWeek().toString().toLowerCase());
        System.out.println(
            "Next one in "
            + ChronoUnit.DAYS.between(LocalDate.now(), dob.withYear(LocalDate.now().getYear() + 1)) + " days"
        );
        System.out.println(
            "Meanwhile in Puerto Rico: " + ZonedDateTime.now().withZoneSameInstant(ZoneId.of("America/Puerto_Rico"))
        );
    }
}
