package chapter10;

class GeneralException extends Exception {
    public GeneralException(String message) {
        super(message);    // We can override the displayed message that way
    }
    private static final long serialVersionUID = 1L;    // This is required for any class that use serialization
}

class CustomException extends GeneralException {
    public CustomException(String message) {
        super(message);
        this.message = message;
    }
    public String getMessage() {    // We can override getMessage to provide more details from the Exception class
        return message + " (" + serialVersionUID + ")";
    }
    private String message;
    private static final long serialVersionUID = 2L;    // Side note, this should be unique so random long is better
}

class EngineException extends Exception {
    public EngineException(String... details) {
    }
    private static final long serialVersionUID = 3L;
}

class Vehicle {
    public Vehicle(int passenger_count, boolean has_engine) throws GeneralException {    // Defines what can be raised
        if (passenger_count < 0) throw new GeneralException("Cannot have negative number of passenger");
        if (passenger_count == 0) throw new CustomException("Must use Drone class if no passenger");    // Polymorphism
        this.passenger_count =  passenger_count;
        this.has_engine = has_engine;
    }
    public Vehicle() {
        passenger_count = 2;
    }
    public void start() throws EngineException {
        if (!has_engine) throw new EngineException(this.manufacturer);    // We can pass any info if we have use for them
    }
    public void stop() throws CustomException {}    // No need to add details for the current example
    int passenger_count;
    boolean has_engine = false;
    String manufacturer = "Alset";
}

public class Exceptional {
    public static void main (String args []) throws GeneralException, EngineException {
        /* We must define errors raised by any called method/constructor. Could be just `Exception` (not recommended) */
        try {
            Vehicle car = new Vehicle(-1, true);    // Handled
            car = new Vehicle();
            car.start();                            // Should fail and be handled but will never be executed
        }
        catch (GeneralException exc)  {
            System.out.println("`" + exc.getMessage() + "` was caught!");
        }    // The block needs to be enclosed witch curly brackets even on a single line
        try {
            Vehicle car = new Vehicle();
            car.start();                            // Handled
            car.stop();
        }
        catch (EngineException exc) {System.out.println("Engine failure");}    // Example of multiple catch
        catch (CustomException exc) {System.out.println(exc.getStackTrace());}
        finally {System.out.println("End of process");}    // This block will be executed regardless of exceptions
        try {
            throw new CustomException("Unknown error");
        }
        catch (CustomException exc) {
            System.out.println("Are we fine?");
            throw exc;    // Throwing back the exception
        }
    }
}
