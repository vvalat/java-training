package chapter11;

class BankAccount extends Thread {
    public synchronized void withdraw(int amount) throws InterruptedException {
        if (amount <= current_balance) {
            current_balance -= amount;
            System.out.println("Withdrawing €" + amount);
            System.out.println("New balance: €" + current_balance);
        }
        else {
            System.out.println(
                "Insufficient funds for withdrawing €" + amount + " while account has €" + current_balance
            );
            wait();    // Semantic: Wait for a new deposit (or a withdrawal that might fail or not)
        }
    }
    public synchronized void deposit(int amount) {
        current_balance += amount;
        System.out.println("Depositing €" + amount);
        System.out.println("New balance: €" + current_balance);
        notifyAll();    // Semantic: inform pending withdrawal that they can try to withdraw
    }
    private int current_balance = 1000;
}

class Withdrawal extends Thread {
    public Withdrawal(BankAccount account, int amount, int delay) {
        this.account = account;
        this.amount = amount;
        this.delay = delay;
    }
    public void run(){
        try {
            while (!interrupted()) {
                account.withdraw(amount);
                sleep(delay);
            }
        }
        catch (InterruptedException exc) {}
        
    }
    private BankAccount account;
    private int amount;
    private int delay;
}

class Deposit extends Thread {
    public Deposit(BankAccount account, int amount, int delay) {
        this.account = account;
        this.amount = amount;
        this.delay = delay;
    }
    public void run(){
        try {
            while (!interrupted()) {
                account.deposit(amount);
                sleep(delay);
            }
        }
        catch (InterruptedException exc) {}
        
    }
    private BankAccount account;
    private int amount;
    private int delay;
}

public class BankingSimulator {
    public static void main(String[] args) {
        BankAccount account = new BankAccount();
        Withdrawal w1 = new Withdrawal(account, 150, 50);
        Withdrawal w2 = new Withdrawal(account, 300, 10);
        Deposit d = new Deposit(account, 200, 100);    // Higher delay between deposits
        w1.setPriority(Thread.MIN_PRIORITY);    // Playing with these should impact occurrences of 'insufficient funds'
        w2.setPriority(Thread.NORM_PRIORITY);
        d.setPriority(Thread.MAX_PRIORITY);    
        w1.start(); w2.start(); d.start();
    }
}
