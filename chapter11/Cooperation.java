package chapter11;

class SyncDisplay {
    public synchronized void displayFoo() throws InterruptedException {    // Exception declaration mandatory
        if (!ready) {    // Foo goes first so wait only for init state of ready or Bar defined one
            System.out.println("Foo");
            System.out.println(count++);    // The lock ensures the class static variable is correctly incremented
            ready = true;                   // Here too
            notifyAll();    // Inform waiting threads that they can resume            
        }
        else wait();
    }
    public synchronized void displayBar() throws InterruptedException {
        if (ready) {
            System.out.println("Bar");
            System.out.println(count++);
            notifyAll();
            ready = false;
        }
        else wait();
    }
    private static int count = 1;
    private boolean ready = false;
}

class DisplayFooThread extends Thread {
    public DisplayFooThread(SyncDisplay display) {
        this.display = display;
    }
    public void run() {
        try {
            while (!interrupted()) {
                display.displayFoo();
                sleep(50);
            }
        }
        catch (InterruptedException exc) {}
    }
    private SyncDisplay display;
}

class DisplayBarThread extends Thread {
    public DisplayBarThread(SyncDisplay display) {
        this.display = display;
    }
    public void run() {
        try {
            while (!interrupted()) {
                display.displayBar();
                sleep(150);
            }
        }
        catch (InterruptedException exc) {}
    }
    private SyncDisplay display;
}

public class Cooperation {
    public static void main(String [] args) {
        SyncDisplay shared = new SyncDisplay();
        DisplayFooThread a = new DisplayFooThread(shared);
        DisplayBarThread b = new DisplayBarThread(shared);
        a.start();
        b.start();  // Use ctrl+c to interrupt
    }
}
