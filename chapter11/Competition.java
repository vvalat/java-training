package chapter11;

import java.util.Scanner;

class PrintSomething extends Thread {
    public PrintSomething(String message) {
        _string = message;
    }
    public void run() {
        try {
            while (true) {    // Infinite loop
                if (interrupted()) return;    // Exit condition, must be implemented
                System.out.println("You say: " + _string);
                sleep(75);    // Allows other threads to pitch in, it may raise interruption exception, see below
            }
        }
        catch (InterruptedException exc) {}    // This is mandatory but the `throws` declaration is handled by Thread
        
    }
    private String _string;
}

class PrintSomethingDifferent {
    public PrintSomethingDifferent(String message) {
        _string = message;
    }
    public void display() {
        System.out.println("I say: " + _string);
    }
    private String _string;
}

class PrintSomethingElse extends PrintSomethingDifferent implements Runnable {    // Equivalent when already extending
    public PrintSomethingElse(String message) {
        super(message);
    }
    public void run() {
        try {
            while (true) {
                if (Thread.interrupted()) return;    // Must call to the static method when not extending Thread
                super.display();
                Thread.sleep(50);    // Same as above
            }
        }
        catch (InterruptedException exc) {}
    }
}

public class Competition {
    public static  void main(String args []) {
        PrintSomething a = new PrintSomething("Foo");
        PrintSomethingElse _b = new PrintSomethingElse("Bar");
        Thread b = new Thread(_b);    // a Runnable interface is not a Thread, must instantiate one
        b.setDaemon(true);    // A daemon thread is interrupted automatically if no user thread is currently running
        a.start();    /* a is a Thread instance (polymorphism) */
        b.start();    /* b is explicitly a Thread instance */
        Scanner input = new Scanner(System.in);
        input.next();    // Cannot detect key stroke in console so need any input + Enter
        input.close();
        a.interrupt();    // Only need to interrupt a, b is a daemon
    }
}
