package chapter12;

import java.util.Random;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;

class Window extends JFrame {
    public Window(int hSize, int vSize, String title) {
        /* Manual centering */
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int hBound = (int) (screen.getWidth() - hSize) / 2;
        int vBound = (int) (screen.getHeight() - vSize) / 2;
        setBounds(hBound, vBound, hSize, vSize);
        /* Window behaviour*/
        setTitle(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // Allow closing the thread on default close action
        /* Populating */
        Container main = getContentPane();    // We retrieve the window container that will hold the components
        main.setLayout(new FlowLayout());    // Layout manager that does flex positioning
        JButton start = new JButton("Start");
        main.add(start);
        panel = new Panel();    // Panel (extends JPanel) is a component that allows graphics to be inserted
        ButtonListener start_listener = new ButtonListener(start, panel, main);
        start.addActionListener(start_listener);
    }
    private static final long serialVersionUID = 1L;
    private Panel panel;
}

class MouseListener extends MouseInputAdapter {    // Allows implementating an interface w/o overriding everything
    public MouseListener (Panel panel, Thread change) {
        this.panel = panel;    // This allows having access to the panel in the context of an event
        this.change = change;    // Thread control for handling restarting count after hit
    }
    public void mousePressed(MouseEvent evt) {    // We override only the event handler that we need
        if (panel.checkHit(evt.getX(), evt.getY())) {
            change.interrupt();
            panel.incrementScore();
            panel.repaint();
            change = new ChangeTarget(panel);
            change.start();
        }
    }
    private Panel panel;
    private Thread change;
}

class ChangeTarget extends Thread {
    public ChangeTarget(Panel panel) {
        this.panel = panel;
    }
    public void run() {
        try {
            while (!interrupted()) {
                sleep(1000);    // Could turn into variable for difficulty adjustment
                panel.repaint();
            }
        }
        catch (InterruptedException exc) {}
    }
    private Panel panel;
}

class ButtonListener implements ActionListener {
    public ButtonListener(JButton button, Panel p, Container c) {    // Needs to interact with almost everything
        this.button = button;
        this.p = p;
        this.c = c;
    }
    public void actionPerformed(ActionEvent evt) {
        if (evt.getActionCommand() == "Start") {
            this.button.setVisible(false);
            p.setPreferredSize(new Dimension(c.getWidth(), c.getHeight()));
            c.add(p);    // Binding the panel to current window context
            ChangeTarget change = new ChangeTarget(p);
            change.start();
            p.addMouseListener(new MouseListener(p, change));    // Allowing listening to different events
        }
    }
    private JButton button;
    private Panel p;
    private Container c;
}

class Panel extends JPanel {
    public Panel() {
        setBackground(Color.ORANGE);
    }
    public void paintComponent(Graphics g) {    // Will be called by `repaint()` to redraw the panel
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        setBackground(Color.ORANGE);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.drawString("Score: " + score, 20, 25);
        /* Pseudo-random positioning of target */
        x = Math.max(20, rnd.nextInt((int) this.getSize().getWidth() - target_size));
        y = Math.max(20, rnd.nextInt((int) this.getSize().getHeight() - target_size));
        g.fillOval(x, y, target_size, target_size);
    }
    public boolean checkHit(int x, int y) {
        return Math.abs(x - this.x) <= target_size && Math.abs(y - this.y) <= target_size;
    }
    public void incrementScore() {
        score += 1;
    }
	private static final long serialVersionUID = 2L;
    private static Random rnd = new Random();
    private static int target_size = 20;    // Could turn into variable for difficulty adjustment
    private int x;
    private int y;
    private int score = 0;
}

public class WhackAHole {
    public static void main(String [] args) {
        Window window = new Window(800, 600, "Whack a Hole");
        window.setVisible(true);
    }
}
